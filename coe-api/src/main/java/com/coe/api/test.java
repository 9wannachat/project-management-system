package com.coe.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.coe.api.model.Faculty;
import com.coe.api.model.ListFaculty;
import com.coe.api.model.Status;
import com.coe.api.model.Test;
import com.coe.api.utility.Database;

@RestController
@RequestMapping("test")
public class test {

	@RequestMapping(method = RequestMethod.GET)
	public String testspring() {
		return ("Hello Spring");
	}

	@RequestMapping(value = "/basic", method = RequestMethod.GET)
	public ResponseEntity<Test> basic() {
		Test test = new Test();

		test.setName("Com Eng");
		test.setAge(9);

		return ResponseEntity.ok().body(test);
	}

	@RequestMapping(value = "/dbbasic", method = RequestMethod.GET)
	public ResponseEntity<ListFaculty> dbBasic() {

		ListFaculty listFaculty = new ListFaculty();

		PreparedStatement prepared = null;
		Connection con = Database.connectDatabase();
		ResultSet result = null;

		try {
			String sql = "SELECT * FROM faculty";
			prepared = con.prepareStatement(sql);

			result = prepared.executeQuery();

			List<Faculty> listFac = new ArrayList<Faculty>();

			while (result.next()) {
				Faculty faculty = new Faculty();

				faculty.setFaculty_id(result.getInt(1));
				faculty.setFaculty_name(result.getString(2));

				listFac.add(faculty);
			}

			listFaculty.setFaculty(listFac);
			listFaculty.setCodeStatus(200);
			listFaculty.setNameStatus("Success");

			Database.closeConnection(con);
			return ResponseEntity.ok().body(listFaculty);

		} catch (SQLException e) {

			listFaculty.setCodeStatus(200);
			listFaculty.setNameStatus(e.getMessage());

			Database.closeConnection(con);
			return ResponseEntity.ok().body(listFaculty);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/fac", method = RequestMethod.POST)
	public ResponseEntity<Status> basicInsert(@RequestBody Faculty faculty) {

		Status status = new Status();
		PreparedStatement prepared = null;
		Connection con = Database.connectDatabase();

		String sql = "insert into faculty (faculty_name) values (?) ";

		try {

			prepared = con.prepareStatement(sql);
			prepared.setString(1, faculty.getFaculty_name());

			boolean statusInsert = prepared.execute();

			if (!statusInsert) {
				status.setCodeStatus(200);
				status.setNameStatus("Create");
			} else {
				status.setCodeStatus(400);
				status.setNameStatus("Unsuccess");
			}

		} catch (SQLException ex) {
			status.setCodeStatus(400);
			status.setNameStatus(ex.getMessage());
		} finally {
			return ResponseEntity.ok().body(status);
		}

	}

}
