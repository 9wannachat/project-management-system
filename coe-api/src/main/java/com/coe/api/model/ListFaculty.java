package com.coe.api.model;

import java.util.List;

public class ListFaculty extends Status {
	private List<Faculty> faculty;

	public List<Faculty> getFaculty() {
		return faculty;
	}

	public void setFaculty(List<Faculty> faculty) {
		this.faculty = faculty;
	}
	
	
}
