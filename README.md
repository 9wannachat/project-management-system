**Project Management System**

> Overview

> เป็นระบบใช้จัดการโปรเจคนักศึกษาในสาขาวิศวกรมคอมพิวเตอร์ คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเทคโนโลยีราชมงคลศรีวิชัย ในปัจจุบันระบบยังเป็น เวอร์ชั่น 1 ซึ่งใช้ PHP, MySQL, HTML, Bootstrap ในการพัฒนา เเต่ตอนนี้ระบบกำลังได้รับการพัฒนาต่อยอดเป็นเวอร์ชั่น 2 โดยใช้ Spring Framwork, MySQL, VUE ในการพัฒนา


>Version 1

![enter image description here](https://lh3.googleusercontent.com/RNCHgP7jjGG5ixc9i8y0N0V0TghF0M_suy4PTG8qF10D5ttxUgOf87Gv4Fwq_l96b9YbGkCIXY0-)

![enter image description here](https://lh3.googleusercontent.com/rJk5pQ7TywOR-9zl65iEitKPk8HtiTnrTSaImE8nW7sE4NZP9bvXxtSl7Rc8rvKrxKI_AXnV0u5t)

![enter image description here](https://lh3.googleusercontent.com/Kq_j-IW_yr3Ol6DSBqSK0thx7TF6ofusmBZ5wwTktSBqpHX8XdF1bE95JjpiOHNSvSRd9XaDYNqV)

> Version 2

![enter image description here](https://lh3.googleusercontent.com/S6fLL5jsAp9ao_rImby1oQSgvEqj56o4JphBdM7B3T1bcx9E8ThhqIB-ZagQs9kMY5ZhSt5URCnx)

![enter image description here](https://lh3.googleusercontent.com/kqTiraxl3EcFHP-lFJ3uhUOuT2_E6_fkH7vqVeH_Rxra0XcS6h4ozORO5vroRoL8X-bEl7lVWQrE)

![enter image description here](https://lh3.googleusercontent.com/9uTLOFOfpNX5tE3nDVHGdoHw-q4o8nFMIEyhIvaNlu_0_KoQtPyKscEtgAsnLeQgvSA_t9ob_wKQ)

