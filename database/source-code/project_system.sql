create database project_system
	character set utf8mb4 collate utf8mb4_general_ci;
    
use project_system;

create table member_status(
	member_status_id tinyint auto_increment primary key,
    member_status_name varchar(20)
);

create table member_priority(
	member_priority_id tinyint auto_increment primary key, 
    member_priority_name varchar(50)
);

create table prefix_name(
	prefix_id int(2) zerofill auto_increment primary key,
    prefix_name varchar(30)
);

create table faculty(
	faculty_id int(2) zerofill auto_increment primary key,
    faculty_name varchar(100)
);

create table department(
	department_id int(2) zerofill auto_increment primary key,
    department_name varchar(100),
    department_fec int(2) zerofill,
    foreign key (department_fec) references faculty(faculty_id)
);

create table course(
	course_id int(2) zerofill auto_increment primary key,
    course_name varchar(50),
    course_dep int(2) zerofill,
    foreign key (course_dep) references department(department_id)
);

create table project (
	project_id varchar(20) primary key,
    project_nameTH text ,
    project_nameEN text,
    project_created_date datetime DEFAULT CURRENT_TIMESTAMP 
);

create table std_member (
	std_member_id varchar(12) primary key,
    std_member_username varchar(50),
    std_member_password varchar(100),    
    std_member_id varchar(12) primary key,
    std_member_prefix int(2) zerofill,
    std_member_fname varchar(50),
    std_member_lname varchar(50),
    std_member_faculty int(2) zerofill,
    std_member_department int(2) zerofill,
    std_member_course int(2) zerofill,
    std_memberl_uni_email varchar(100),
    std_member_per_email varchar(100),
    std_member_tel varchar(10),
    std_member_line varchar(30),
    std_member_fb varchar(250),
    std_member_priority tinyint,
    std_member_verify tinyint default 1,
    std_member_create datetime DEFAULT CURRENT_TIMESTAMP,
    foreign key (std_member_verify) references member_status(member_status_id)
);

create table std_detail (
	std_detail_id varchar(12) primary key,
    std_detail_prefix int(2) zerofill,
    std_detail_fname varchar(50),
    std_detail_lname varchar(50),
    std_detail_faculty int(2) zerofill,
    std_detail_department int(2) zerofill,
    std_detail_course int(2) zerofill,
    std_detail_uni_email varchar(100),
    std_detail_per_email varchar(100),
    std_detail_tel varchar(10),
    std_detail_line varchar(30),
    std_detail_fb varchar(250),
    std_detail_priority tinyint       
);